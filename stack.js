/*

# Day 341: 2017-Dec-08 (Fri):

Given a .blocks file, stack the blocks so that each position in the basis
genome points to a position in another genome (or many positions).

*/
const fs = require('fs');
const itree = require('interval-tree-1d');

function notice(msg) { console.error(Date(), msg); }
function exists(fpath) { return fs.existsSync(fpath) && fs.statSync(fpath).isFile(); }

// filter a list of stacked pointers to only include those within an annotation set
function stack(ifile_blocks)
{
	if (!exists(ifile_blocks)) return;

	notice('loading');
	let records = JSON.parse(fs.readFileSync(ifile_blocks));
	let length = records[0].from[2];
	let links = new Array(length);

	notice('processing');
	for (let i=0; i<length; ++i) {
		links[i] = [];
	}

	for (let {from, to, distance, blocks} of records)
	{
		let [idA, headerA, lengthA] = from;
		let [idB, headerB, lengthB] = to;

		for (let [oset,posA,posB,len,score] of blocks)
		{
			for (let i=0; i<len; ++i) {
				links[posA].push(posB);
				if (++posA == lengthA) posA = 0;
				if (++posB == lengthB) posB = 0;
			}
		}
		break;
	}

	notice('writing');
	for (let i=0; i<links.length; ++i)
	{
		console.log(i + "\t" + links[i].join(","));
	}

	notice('finished');
}
module.exports = stack;

// CLI
let docs = `{

Usage:
  node stack <ifile_blocks>
Example:
  node stack ./a_thaliana.blocks > ./a_thaliana_filtered.links

}`;
if (require.main === module)
{
	if (process.argv.length != 3) {
		console.log(docs);
		process.exit();
	}
	stack(
		process.argv[2] // file blocks
	);
}

// let ifile = '/Users/julian/Desktop/Code/javascript/readslam/1kp/211_genbank_to_features/data/Acacia-ligulata.genbank';
// let genbank_file = "/Users/julian/Desktop/Code/javascript/readslam/1kp/data/refseq_cpt/Arabidopsis-thaliana.genbank";
// let ifile = '/Users/julian/Desktop/Code/javascript/readslam/1kp/211_genbank_to_features/data/Acacia-ligulata.genbank';

// extract(ifile);
