/*

# Day 341: 2017-Dec-08 (Fri):

Given a stacked file of pointers, filter it to stay within a provided list of
annotations. (GenBank or GFF format)

*/
const fs = require('fs');
const itree = require('interval-tree-1d');

const LineReader = require('./lib/line_reader');
const LineWriter = require('./lib/line_writer');
const GenbankReader = require('./lib/reader_genbank');

function notice(msg) { console.error(Date(), msg); }
function stdout(msg) { console.log(msg); }
function exists(fpath) { return fs.existsSync(fpath) && fs.statSync(fpath).isFile(); }
function assert(test,text) { console.assert(test,text); }

// load feature boundaries from a genbank file
function load_features(ifile)
{
	let record = GenbankReader(ifile)();
	let segs = [];

	for (let feature of record.features)
	{
		if (feature.type != 'CDS' && feature.type != 'tRNA' && feature.type != 'rRNA') continue;

		for (let seg of feature.segs)
		{
			// posA, posB, id, type, name, strand. NOTE: positions become 0-based
			segs.push([
				seg.posA - 1,
				seg.posB - 1,
				// feature.id,
				// feature.type,
				feature.name
				// seg.fwd ? '+' : '-'
			]);
		}
	}
	return itree(segs);
}

// filter a list of stacked pointers to only include those within an annotation set
function filter(ifile_genbank, ifile_stacks)
{
	if (!exists(ifile_genbank)) return;
	if (!exists(ifile_stacks)) return;

	// load data from the genbank file
	let features = load_features(ifile_genbank);

	let reader = new LineReader(ifile_stacks);

	for (let line = reader.next(), linenum=0; line !== null; line = reader.next(), ++linenum)
	{
		features.queryPoint(linenum, val => {
			console.log(line + "\t" + val[2]);
		});
	}
}
module.exports = filter;

// CLI
let docs = `{

Usage:
  node filter <ifile_genbank> <ifile_links>

Example:
  node filter ./a_thaliana.gbff ./a_thaliana.links > ./a_thaliana_filtered.links

}`;
if (require.main === module)
{
	if (process.argv.length != 4) {
		console.log(docs);
		process.exit();
	}
	filter(
		process.argv[2], // file genbank
		process.argv[3]  // file links
	);
}

// let genbank_file = "/Users/julian/Desktop/Code/javascript/readslam/1kp/data/refseq_cpt/Arabidopsis-thaliana.genbank";
// let ifile = '/Users/julian/Desktop/Code/javascript/readslam/1kp/211_genbank_to_features/data/Acacia-ligulata.genbank';
// extract(ifile);
