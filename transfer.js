/*

# Day 341: 2017-Dec-08 (Fri):

Given an annoted genome A (genbank) and one or many unannotated genomes B (fasta),
align A into B to get alignment links, then transfer annotations from A to B.

See: https://www.npmjs.com/package/augmented-interval-tree for itree docs.

*/

const fs = require('fs');
const IntervalTree = require('augmented-interval-tree');
const LineReader = require('./lib/line_reader');
const LineWriter = require('./lib/line_writer');
const GenbankReader = require('./lib/reader_genbank');
const DNA = require('./lib/dna_utils');
const Index = require('./lib/multi_index');
const align = require('./lib/align');

function notice(msg) {
	console.error(Date(), msg);
}

// load the first record from a genbank file
function load_genbank(ifile)
{
	let record = GenbankReader(ifile)();
	let itree = new IntervalTree();

	for (let feature of record.features)
	{
		if (feature.type == 'CDS' || feature.type == 'tRNA' || feature.type == 'rRNA' || feature.type == 'intron')
		{
			for (let seg of feature.segs)
			{
				// NOTE: positions are converted to 0-based here
				itree.insert(seg.posA - 1, seg.posB - 1, [
					feature.id,
					feature.type,
					feature.name,
					seg.fwd ? '+' : '-',
					seg.posA - 1,
					seg.posB - 1
				]);
			}
		}
	}
	return {
		metadata : record.metadata,
		sequence : record.sequence,
		features : itree,
		length : record.sequence.length
	};
}

// load the first record from a fasta file
function load_fasta(ifile)
{
	let records = DNA.load_fasta(ifile);

	return records.map(record => ({
		metadata : record.meta,
		sequence : record.data,
		features : new IntervalTree(),
		length : record.data.length
	}));
}

// given an annotations IntervalTree and a set of alignment blocks, generated transferred annotation blocks
function transfer(itree, alignments)
{
	let transfers = [];

	// for each alignment block
	for (let [posA, strandB, posB, len] of alignments)
	{
		let endA = posA + len - 1;

		// get the annotations (segments) that intersect with this alignment block
		let segs = itree.find(posA,endA);

		// for each of the segments
		for (let {start,end,data} of segs)
		{
			let [seg_id, seg_type, seg_name, seg_strand, seg_pos, seg_end] = data;

			// clip the edges of the annotation so that it fits within the alignment block
			let clipA = seg_pos < posA ? posA : seg_pos;
			let clipB = seg_end > endA ? endA : seg_end;

			// convert to offsets relative to the start of the alignment block
			let relA = clipA - posA;
			let relB = clipB - posA;

			// transfer the clipped annotation
			transfers.push({
				type : seg_type,
				name : seg_name,

				strA : seg_strand,
				begA : seg_pos,
				endA : seg_end,
				lenA : seg_end - seg_pos + 1,

				clipA : seg_pos - clipA,
				clipB : clipB - seg_end,

				strB : strandB,
				begB : posB + relA,
				endB : posB + relB,
				lenB : relB - relA + 1
			});
		}
	}
	return transfers;
}

function main(ifileA, ifileB)
{
	// load A
	let genomeA = load_genbank(ifileA);

	// load B
	let genomesB = load_fasta(ifileB);

	for (let genomeB of genomesB)
	{
		//let genomeB = load_genbank(ifileB);
		notice(`processing ${genomeB.metadata}`);

		// align A into B (alignment blocks and pointers)
		let blocks = align(genomeA.sequence, genomeB.sequence, 50);

		// transfer annotations
		let transfers = transfer(genomeA.features, blocks).sort( (a,b) => {
			if (a.type < b.type) return -1;
			if (a.type > b.type) return 1;
			if (a.name < b.name) return -1;
			if (a.name > b.name) return 1;
			if (a.strA < b.strA) return -1;
			if (a.strA > b.strA) return 1;
			if (a.strB < b.strB) return -1;
			if (a.strB > b.strB) return 1;
			return a.begB - b.begB;
		});

		for (let t of transfers)
		{
			console.log([
				t.type,
				t.name,

				// block in the basis genome (strand, position, length)
				[
					t.strA,
					t.begA,
					//t.endA,
					t.endA - t.begA + 1
				].join(','),

				// block in the other genome
				[
					t.strB,
					t.begB,
					//t.endB,
					t.endB - t.begB + 1
				].join(','),

				// clipped edges of the basis block
				[
					t.clipA < 0 ? t.clipA : 0,
					t.clipB < 0 ? t.clipB : 0
				].join(','),

				// other genome name
				genomeB.metadata

			].join("\t"));
		}
	}
}

// CLI
let docs = `{

Given an annotated genome A (GenBank format) and an unannotated genome B (fasta),
align A to B and transfer annotations.

Usage:
  node transfer <ifile_genbank> <ifile_fasta>

Example:
  node transfer ./a_thaliana.gbff ./unknown.fasta > ./annotations.tsv

}`;
if (require.main === module)
{
	if (process.argv.length != 4) {
		console.log(docs);
		process.exit();
	}
	main(
		process.argv[2], // file genbank
		process.argv[3]  // file fasta
	);
}

//let ifileA = "/Users/julian/Desktop/Code/javascript/readslam/1kp/data/refseq_cpt/Arabidopsis-thaliana.genbank";
//let ifileB = '/Users/julian/Desktop/Code/javascript/readslam/1kp/data/refseq_cpt/Acacia-ligulata.genbank';
//let ifileB = "/Users/julian/Desktop/Code/javascript/readslam/1kp/data/sequences_cpt/gb_AP000423.1_Arabidopsis_thaliana.fa";
//let ifileB = "/Users/julian/Desktop/Code/javascript/readslam/1kp/data/sequences_cpt/gb_KT992850.1_Oryza_brachyantha.fa";

//main(ifileA, ifileB);

//node transfer ../data/refseq_cpt/Arabidopsis-thaliana.genbank ../data/sequences_cpt/gb_KT992850.1_Oryza_brachyantha.fa
//node transfer ../data/refseq_cpt/Arabidopsis-thaliana.genbank ../data/refseq_cpt/Arabidopsis-thaliana.genbank
//node transfer ../data/refseq_cpt/Arabidopsis-thaliana.genbank /Users/julian/Desktop/Code/javascript/readslam/1kp/data/sequences_cpt/gb_AP000423.1_Arabidopsis_thaliana.fa
