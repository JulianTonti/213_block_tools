/*

# Day 340: 2017-Dec-07 (Thu):

Extract feature boundaries from a genbank formatted file.

[
	species
id, type, name, strand, start, length

*/
const ReaderGB = require('./reader_genbank');

// extract features from the first record in a genbank file
function extract(ifile)
{
	let next = ReaderGB(ifile);
	let segs = [];

	while (record = next())
	{
		for (let feature of record.features)
		{
			if (feature.type != 'CDS' && feature.type != 'tRNA' && feature.type != 'rRNA') continue;

			for (let seg of feature.segs)
			{
				segs.push([
					feature.id,
					feature.type,
					feature.name,
					seg.fwd ? '+' : '-',
					seg.posA - 1,
					seg.posB - seg.posA + 1
				]);
			}
		}
		break;
	}
	return segs;
}
module.exports = extract;

// { id: 98,
//    type: 'CDS',
//    text: '59964..61391',
//    name: 'rbcL',
//    fwd: true,
//    segs: [ [Object] ],
//    posA: 59964,
//    posB: 61391,
//    '/gene': 'rbcL',
//    '/codon_start': '1',
//    '/transl_table': '11',
//    '/product': 'RbcL; ribulose-1,5-bisphosphate carboxylase/oxygenase large subunit',
//    '/protein_id': 'CED95169.1',

if (require.main === module)
{
	let ifile = '/Users/julian/Desktop/Code/javascript/readslam/1kp/211_genbank_to_features/data/Acacia-ligulata.genbank';
	extract(ifile);
}
