function sync(gs) {
	let g = gs( (...a) => setImmediate( () => g.next(a) ) );
	g.next();
}
module.exports = sync;
