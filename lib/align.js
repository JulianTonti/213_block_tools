/*

# Day 341: 2017-Dec-08 (Fri):

*/
const wrap = require('./wrap');
const Index = require('./multi_index');
const DNA = require('./dna_utils');

const cA = 'A'.charCodeAt(0);
const cT = 'T'.charCodeAt(0);
const cC = 'C'.charCodeAt(0);
const cG = 'G'.charCodeAt(0);

// return the score between two positions
function score_link(seqA, seqB, posA, posB)
{
	let base1 = wrap.get(seqA, posA);
	let base2 = wrap.get(seqB, posB);

	return base1 == base2 ? 1 : 0;

	switch (base1) {
		case cA:
			switch (base2) {
				case cA: return 2;
				case cT: return 0;
				case cC: return -4;
				case cG: return 1;
				default: return 0;
			}
		case cT:
			switch (base2) {
				case cA: return 0;
				case cT: return 2;
				case cC: return 1;
				case cG: return -4;
				default: return 0;
			}
		case cC:
			switch (base2) {
				case cA: return -4;
				case cT: return 0.5;
				case cC: return 4;
				case cG: return 0;
				default: return 0;
			}
		case cG:
			switch (base2) {
				case cA: return 0.5;
				case cT: return -4;
				case cC: return 0;
				case cG: return 4;
				default: return 0;
			}
		default: return 0;
	}
}

// get a score for a site and both its flanks
function score_flanks(seqA, seqB, posA, posB, flanksize=50)
{
	let score = score_link(seqA, seqB, posA, posB);

	for (let i=1; i<=flanksize; i++) {
		score += score_link(seqA, seqB, posA+i, posB+i);
		score += score_link(seqA, seqB, posA-i, posB-i);
	}
	return score;
}

// calculate an updated score if a pointer were bumped one position downstream
function peek_right(seqA, seqB, posA, posB, current_score=0, flanksize=50)
{
	return (current_score
		+ score_link(seqA, seqB, posA + flanksize + 1, posB + flanksize + 1)
		- score_link(seqA, seqB, posA - flanksize    , posB - flanksize    )
	);
}

// calculate an updated score if a pointer were bumped one position upstream
function peek_left(seqA, seqB, posA, posB, current_score=0, flanksize=50)
{
	return (current_score
		+ score_link(seqA, seqB, posA - flanksize - 1, posB - flanksize - 1)
		- score_link(seqA, seqB, posA + flanksize    , posB + flanksize    )
	);
}

// align two sequences and return alignment blocks
function align(seq_basis, seq_other, flanksize=50)
{
	// build sequences
	let seqs = {
		'g' : new Buffer(seq_basis),
		'+' : new Buffer(seq_other),
		'-' : DNA.revcomp(seq_other)
	};
	let genome_length = seq_basis.length;

	// build the multi-index for lookups
	let index = new Index();
	index.add_sequence('+', seqs['+']);
	index.add_sequence('-', seqs['-']);

	/*
		PHASE 1: search for alignment blocks
	*/

	// represents a pointer between two sequences from A to B
	let idA = 'g';
	let idB = '';
	let posA = 0;
	let posB = 0;
	let score = 0;
	let lcplen = 0;

	// data structure for affix array alignment blocks
	let blocks = []; // [idA, posA, idB, posB, lcp_len]

	// populate blocks by aligning from the basis genome into the multi-index
	for (let offset=0; offset<genome_length; ++offset)
	{
		// get the best alignment at this offset (can return multiple results)
		let alignments = index.search(seqs.g, offset); // {id,pos,min,max,len}

		if (alignments.length == 0 || alignments[0].len == 0) continue;

		// expand all alignments into pointers between sequences
		for (let alignment of alignments)
		{
			idB = alignment.id;
			posA = alignment.pos; // can be less than 'offset' due to affix-array backtracking

			// map each rank from the index lookup back to a position
			for (let rank=alignment.min; rank<=alignment.max; ++rank)
			{
				posB = index.rank_to_posn(idB, rank);

				// record the block
				blocks.push([idA, posA, idB, posB, alignment.len]);
			}
		}

		// jump ahead to the end of the alignment
		let end = alignments[0].pos + alignments[0].len;
		if (end > offset) offset = end - 1;
	}


	/*
		PHASE 2: convert alignment blocks into arrays of single base pointers
		and retain the best pointer(s) at each site in the basis genome
	*/

	// data structures for links between sequences
	let hiscores = new Array(genome_length).fill(0);
	let pointers = new Array(genome_length);

	for (let i=0; i<genome_length; ++i)
	{
		hiscores[i] = 0;  // best score seen so far at this site
		pointers[i] = []; // pointers to destinations in the form [id,pos]
	}

	// write best pointers by alignment length for each of the alignment blocks
	for (let [idA, posA, idB, posB, lcplen] of blocks)
	{
		let score = score_flanks(seqs[idA], seqs[idB], posA, posB, flanksize); // score for the 5' link in the alignment

		// compute scores for all links downstream in the alignment and update the pointers table as needed
		for (let i=0; i<lcplen; ++i)
		{
			// clear the existing pointer if it's obsolete
			if (hiscores[posA] < score) {
				hiscores[posA] = score;
				pointers[posA] = [];
			}

			// add the new pointer if it scores >= the existing score
			if (hiscores[posA] == score) {
				pointers[posA].push([idB, posB]);
			}

			// advance the pointer one base to the right
			score = peek_right(seqs[idA], seqs[idB], posA, posB, score, flanksize);

			// wrap around if needed
			if (++posA == seqs[idA].length) posA = 0;
			if (++posB == seqs[idB].length) posB = 0;
		}
	}


	/*
		PHASE 3: extend pointers upstream and downstream to clobber inferior
		links with superior ones (scored by match-score within the user-defined
		sequence flanks for each site).
	*/

	// extend pointers downstream
	for (let idA='g',posA=0; posA<genome_length; ++posA)
	{
		let next_pos = posA + 1;
		if (next_pos == seqs[idA].length) next_pos = 0; // wrap around

		let curr_score = hiscores[posA];
		let next_score = hiscores[next_pos];

		let peek_score = 0;
		let updating = false; // flag to trigger updating

		for (let [idB,posB] of pointers[posA])
		{
			let peek_score = peek_right(seqs[idA], seqs[idB], posA, posB, curr_score, flanksize);

			// skip if inferior
			if (peek_score < next_score) {
				continue;
			}

			// clear if superior
			if (peek_score > next_score)
			{
				updating = true;
				next_score = peek_score;
				hiscores[next_pos] = next_score;
				pointers[next_pos] = [];
			}

			// to get here, scores are the same. Only update if flagged
			if (updating)
			{
				if (++posB == seqs[idB].length) posB = 0; // wrap around
				pointers[next_pos].push([idB,posB]);
			}
		}
	}

	// extend pointers upstream
	for (let idA='g',posA=genome_length-1; posA>=0; --posA)
	{
		let next_pos = (posA == 0) ? seqs[idA].length - 1 : posA - 1;
		let curr_score = hiscores[posA];
		let next_score = hiscores[next_pos];
		let peek_score = 0;
		let updating = false;

		for (let [idB,posB] of pointers[posA])
		{
			let peek_score = peek_left(seqs[idA], seqs[idB], posA, posB, curr_score, flanksize);

			// skip if inferior
			if (peek_score < next_score) {
				continue;
			}

			// clear if superior
			if (peek_score > next_score)
			{
				updating = true;
				next_score = peek_score;
				hiscores[next_pos] = next_score;
				pointers[next_pos] = [];
			}

			// to get here, scores are the same. Only update if flagged
			if (updating)
			{
				if (--posB == -1) posB = seqs[idB].length - 1;
				pointers[next_pos].push([idB,posB]);
			}
		}
	}

	// compress the pointers into blocks TODO: include wrap around
	let final = []; // alignment blocks to be returned
	let stack = []; // temporary stack for building alignment blocks

	// push new elements into the stack
	for (let posA=0; posA<genome_length; ++posA)
	{
		// reset stack elements
		for (let s of stack) {
			s.touched = false;
		}

		// extend stack elements if possible
		for (let [idB,posB] of pointers[posA])
		{
			let absorbed = false;

			for (let s of stack)
			{
				if (s.idB != idB) continue;
				if (s.posB + s.length != posB) continue;
				absorbed = true;
				s.touched = true;
				s.length += 1;
			}

			// start a new stack element if needed
			if (!absorbed)
			{
				stack.push({
					touched : true,
					idA : idA,
					idB : idB,
					posA : posA,
					posB : posB,
					length : 1
				});
			}
		}

		// write finalised stack elements to 'final' and keep others
		let kept = [];

		for (let s of stack)
		{
			if (s.touched) {
				kept.push(s);
			} else {
				final.push([
					s.posA,  // position from
					s.idB,   // id (typically strand) to
					s.posB,  // position to
					s.length // alignment length
				]);
			}
		}
		stack = kept;
	}

	// flush the remaining stack
	for (let s of stack)
	{
		final.push([
			s.posA,
			s.idB,
			s.posB,
			s.length
		]);
	}
	return final;
}
module.exports = align;
