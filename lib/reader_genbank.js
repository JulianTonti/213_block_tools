/*

# Day 298: 2016-Oct-25 (Tue):

Given a genbank file, extract information from it in more helpful formats.

NOTE: check the release notes for the most recent version of GenBank for
information about GenBank file format. At the time of writing, the most
recent version is 215.

See: ftp://ftp.ncbi.nlm.nih.gov/genbank/release.notes/gb215.release.notes

	3.4.2  Entry Organization

	  In the second portion of a sequence entry file (containing the
	sequence entries for that division), each record (line) consists of
	two parts. The first part is found in positions 1 to 10 and may
	contain:

	1. A keyword, beginning in column 1 of the record (e.g., REFERENCE is
	a keyword).

	2. A subkeyword beginning in column 3, with columns 1 and 2 blank
	(e.g., AUTHORS is a subkeyword of REFERENCE). Or a subkeyword beginning
	in column 4, with columns 1, 2, and 3 blank (e.g., PUBMED is a
	subkeyword of REFERENCE).

	3. Blank characters, indicating that this record is a continuation of
	the information under the keyword or subkeyword above it.

	4. A code, beginning in column 6, indicating the nature of an entry
	(feature key) in the FEATURES table; these codes are described in
	Section 3.4.12.1 below.

	5. A number, ending in column 9 of the record. This number occurs in
	the portion of the entry describing the actual nucleotide sequence and
	designates the numbering of sequence positions.

	6. Two slashes (//) in positions 1 and 2, marking the end of an entry.

	The second part of each sequence entry record contains the information
	appropriate to its keyword, in positions 13 to 80 for keywords and
	positions 11 to 80 for the sequence.

*/
const LineReader = require('./line_reader');
const verbose = false;

// parse a genbank file
function parse(ifile)
{
	let reader = new LineReader(ifile);

	return function next()
	{
		let lines = [];

		for (let line = reader.next(); line !== null; line = reader.next())
		{
			line = line.trimRight();
			if (line.length == 0) continue;
			lines.push(line);
			if (line.substr(0,2) == '//') {
				return parse_single(lines.join("\n"));
			}
		}
		return null;
	}
}

// parse a GenBank record to extract data as a JS object
function parse_single(record)
{
	let extracted = {
		metadata : {},
		features : [],
		sequence : ''
	};
	let regx = null;

	// extract metadata
	regx = record.match(/^(LOCUS[\s\S]*)FEATURES/);
	(regx === null
		? console.error('unable to extract metadata')
		: extracted.metadata = process_metadata(regx[1])
	);

	// extract features
	regx = record.match(/FEATURES[\s\S]*ORIGIN/);
	(regx === null
		? console.error('unable to extract features')
		: extracted.features = process_features(regx[0])
	);

	// extract the DNA sequence
	regx = record.match(/ORIGIN[\s\S]*\/\//);
	(regx === null
		? console.error('unable to extract sequence')
		: extracted.sequence = process_sequence(regx[0])
	);

	return extracted;
}

// extract fields from a GenBank header section
function process_metadata(text)
{
	/*
		LOCUS       LISOD                    756 bp    DNA     linear   BCT 30-JUN-1993
		DEFINITION  Listeria ivanovii sod gene for superoxide dismutase.
		ACCESSION   X64011 S78972
		VERSION     X64011.1  GI:44010
		KEYWORDS    sod gene; superoxide dismutase.
		SOURCE      Listeria ivanovii
		  ORGANISM  Listeria ivanovii
		            Bacteria; Firmicutes; Bacillales; Listeriaceae; Listeria.
		REFERENCE   1  (bases 1 to 756)
		  AUTHORS   Haas,A. and Goebel,W.
		  TITLE     Cloning of a superoxide dismutase gene from Listeria ivanovii by
		            functional complementation in Escherichia coli and characterization
		            of the gene product
		  JOURNAL   Mol. Gen. Genet. 231 (2), 313-322 (1992)
		  MEDLINE   92140371
		REFERENCE   2  (bases 1 to 756)
		  AUTHORS   Kreft,J.
		  TITLE     Direct Submission
		  JOURNAL   Submitted (21-APR-1992) J. Kreft, Institut f. Mikrobiologie,
		            Universitaet Wuerzburg, Biozentrum Am Hubland, 8700 Wuerzburg, FRG
		FEATURES             Location/Qualifiers
	*/

	// merge metadata into an object
	// for (let meta of metadata)
	// {
	// 	if (extracted.metadata[meta.key] === undefined) {
	// 		extracted.metadata[meta.key] = [];
	// 	}
	// 	extracted.metadata[meta.key].push(meta);
	// }
	//
	// let name = extracted.metadata.DEFINITION[0].val.split(' ').slice(0,2).join('_');


	// extract metadata as arrays
	function extract(text)
	{
		let records = [];

		for (let line of text.split("\n"))
		{
			let spaces = line.search(/\S/);
			let key = line.slice(0,12).trim();
			let val = line.slice(12).trim();
			if (!key && !val) continue;

			if (spaces == 0) {
				records.push([key,val]);
			}
			else if (spaces == 12) {
				records[records.length-1][1] += ' ' + val;
			}
			else {
				if (records[records.length-1][2] == undefined) {
					records[records.length-1][2] = [];
				}
				records[records.length-1][2].push([key,val]);
			}
		}
		return records;
	}

	// collapse metadata to a tree
	function collapse(node,array)
	{
		for (let [key,val,sub] of array)
		{
			if (node[key] === undefined) node[key] = [];
			let obj = {};
			node[key].push(obj);
			obj.VALUE = val;
			if (sub) collapse(obj,sub);
		}
	}

	// consolidate metadata tree
	function consolidate(node)
	{
		for (let key of Object.keys(node))
		{
			if (node[key].length == 1) {
				if (Object.keys(node[key][0]).length == 1) {
					node[key] = node[key][0].VALUE;
				}
				else {
					node[key] = node[key][0];
					consolidate(node[key]);
				}
			}
			else {
				for (let obj of node[key]) consolidate(obj);
			}
		}
	}

	let tree = {};
	collapse(tree,extract(text));
	consolidate(tree);
	return tree;
}

// extract records from a GenBank FEATURES section
function process_features(text)
{
	/*
	See: http://www.insdc.org/files/feature_table.html

		4.3 Data item positions

		The position of the data items within the feature descriptor line is as follows:
		column position    data item
		---------------    ---------

		1-5                blank
		6-20               feature key
		21                 blank
		22-80              location

	FEATURES             Location/Qualifiers
	     source          1..521168
	                     /organism="Floydiella terrestris"
	                     /organelle="plastid:chloroplast"
	                     /mol_type="genomic DNA"
	                     /culture_collection="UTEX:1709"
	                     /db_xref="taxon:51328"
	     gene            3257..5593
	                     /gene="rrs"
	                     /locus_tag="FlteC_r001"
	                     /db_xref="GeneID:9481913"
	     rRNA            join(3257..3719,3977..3999,4339..4499,4756..5593)
	                     /gene="rrs"
	                     /locus_tag="FlteC_r001"
	                     /product="16S ribosomal RNA"
	                     /db_xref="GeneID:9481913"
	*/
	// parse each feature into an object and build a set of genes
	let records = [];
	let id = 0;

	// use a regular expression and loop through each record
	let regex_sections = text.match(/^ {5}[^0-9 ].*(\n {21}.*)*/gm);

	if (regex_sections == null) {
		return records;
	}

	for (let raw of regex_sections)
	{
		// object to be populated with data from the regex hit
		let record = {
			id    : 0,    // unique ID
			type  : '',   // the GenBank key, eg: 'gene', 'CDS', 'exon'
			text  : '',   // the original GenBank segment string
			name  : '',   // a feature label from /gene or /locus_tag or 'N/A'
			fwd   : true, // true if the overall feature is on the sense strand
			segs  : [],   // sub-sections of the feature (eg, exons of a gene)
			posA  : 0,    // feature start position relative to 5' sense strand
			posB  : 0     // feature end position relative to 5' sense strand
			//...         // NOTE: other GB fields with keys prefixed by a '/'
		};

		// split and clean the lines of the regex hit
		let lines = [];

		for (let line of raw.split("\n")) {
			line = line.trim();
			if (line.length == 0) continue;
			if (line[0] == '/' || lines.length == 0) {
				lines.push(line);
			}
			else {
				lines[lines.length-1] += ' ' + line;
			}
		}

		// separate the header (first line) and extract the GB type and segment string from it with regex
		let [match,type,segstring] = lines.shift().match(/(\S+)\s+(.*)/);

		// assign the record ID
		record.id = id++;

		// assign the record type
		record.type = type;

		// assign the original text of the segment string
		record.text = segstring;

		// assign the record direction (it's fwd unless the segstring starts with 'complement')
		record.fwd = segstring[0] != 'c';

		// extract data from the segment string and assign to the record
		record.segs = segstring.split(',').map(seg => ({
			fwd  : record.fwd && seg.search('complement') == -1,
			posA : parseInt(seg.split('..')[0].match(/[0-9]+/)[0]),
			posB : parseInt(seg.split('..').pop().match(/[0-9]+/)[0])
		}));

		// assign the outer bounds of the feature
		record.posA = record.segs[0].posA;
		record.posB = record.segs[record.segs.length-1].posB;

		// extract all other data fields for the record
		for (let line of lines)
		{
			console.assert(line[0] == '/'); // fields must begin with a / character

			// extract the key and value
			let eqpos = line.search('=');
			let key = eqpos == -1 ? line : line.substr(0,eqpos).trim();
			let val = eqpos == -1 ? undefined : line.substr(eqpos+1).trim();

			// some fields are flags only
			if (val === undefined) {
				record[key] = true;
				continue;
			}

			// remove spaces from translations
			if (key == '/translation') {
				val = val.split(' ').join('');
			}

			// remove quotes around text values
			if (val[0] == '"' || val[0] == "'") {
				console.assert(val[0] == val[val.length-1],line);
				val = val.slice(1,val.length-1);
			}

			// assign the value directly if not already occupied
			if (record[key] === undefined)	{
				record[key] = val;
			}
			// otherwise, convert to an array (if not already done), then append
			else {
				if (Array.isArray(record[key]) == false) {
					if (key != '/db_xref' && verbose) console.warn("Warning: conversion to array for key:",key);
					record[key] = [record[key]];
				}
				record[key].push(val);
			}
		}

		// finally, assign the record's label from either /gene or /locus_tag
		record.name = record['/gene'] || record['/locus_tag'] || record['/standard_name'] || 'N/A';

		// construction complete
		records.push(record);
	}
	return records;
}

// extract the DNA sequence from a GenBank ORIGIN section
function process_sequence(text)
{
	/*
	ORIGIN
	        1 aaatttttga cttgtaaata ttattaaacc caaaaaaaaa ctttcacatt caaaaagaaa
	       61 tagaaacact taatttttta gaaactttat cttctattag ggaaaagttt ttttgtacaa
	      121 gaattctctt gttatcatct tctcattgtt tttgaaaaag aagaatgcgt gaaaacaaaa
	      181 tttaaataaa aaatgtgaaa atacatttct ttcttttttt catttctttt atttcaaaaa
	//
	*/
	let seqs = [];

	for (let line of text.split("\n"))
	{
		seqs.push(line.trim().split(" ").slice(1).join('').toUpperCase());
	}
	return seqs.join('');
}
module.exports = parse;
