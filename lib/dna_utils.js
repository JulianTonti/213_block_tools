/*

# Day 199: 2017-Jul-19 (Wed):

Some DNA handling utilities

*/
const LineReader = require('./line_reader');

// common character codes
const cR = 'R'.charCodeAt(0);
const cY = 'Y'.charCodeAt(0);
const cA = 'A'.charCodeAt(0);
const cT = 'T'.charCodeAt(0);
const cC = 'C'.charCodeAt(0);
const cG = 'G'.charCodeAt(0);

// reverse complement a DNA sequence
let revcomp = new function()
{
	/*
		Reverse complement an 8-bit DNA sequence. Case is preserved
	*/
	let table = new Buffer(256);

	// invalid letters complement to themselves
	for (let i=0; i<256; ++i) table[i] = i;

	// set valid complements
	for (let pair of ['AT','CG','SW','RY','KM','BV','DH'])
	{
		// upper case
		table[pair.charCodeAt(0)] = pair.charCodeAt(1);
		table[pair.charCodeAt(1)] = pair.charCodeAt(0);

		// lower case
		table[pair.toLowerCase().charCodeAt(0)] = pair.toLowerCase().charCodeAt(1);
		table[pair.toLowerCase().charCodeAt(1)] = pair.toLowerCase().charCodeAt(0);
	}

	return function revcomp(seq)
	{
		let buf = new Buffer(seq);
		buf.reverse();
		for (let i=0; i<buf.length; ++i) buf[i] = table[buf[i]];
		return buf;
	}
}

let clean = new function()
{
	/*
		Clean a DNA sequence, converting illegal characters to N. Case is preserved.
		Returns a count of unknown characters.

		http://www.bioinformatics.org/sms2/iupac.html

		Nucleotide Code:  Base:
		----------------  -----
		A.................Adenine
		C.................Cytosine
		G.................Guanine
		T (or U)..........Thymine (or Uracil)
		R.................A or G
		Y.................C or T
		S.................G or C
		W.................A or T
		K.................G or T
		M.................A or C
		B.................C or G or T
		D.................A or G or T
		H.................A or C or T
		V.................A or C or G
		N.................any base
		. or -............gap

	*/
	let table = new Buffer(256);

	// invalid letters clean to N
	for (let i=0; i<256; ++i) table[i] = 'N'.charCodeAt(0);

	// set valid letters
	for (let upper of 'ACGTURYSWKMBDHVN.-')
	{
		let lower = upper.toLowerCase();
		table[upper.charCodeAt(0)] = upper.charCodeAt(0);
		table[lower.charCodeAt(0)] = lower.charCodeAt(0);
	}

	return function clean(seq)
	{
		let buf = new Buffer(seq);
		for (let i=0; i<buf.length; ++i) buf[i] = table[buf[i]];
		return buf;
	}
}


// recode an upper case DNA sequence to AGY
function recode_agy(dna)
{
	let ret = new Buffer(dna);

	for (let i=0; i<ret.length; ++i)
	{
		switch (ret[i]) {
			case cT: ret[i] = cY; break;
			case cC: ret[i] = cY; break;
		}
	}
	return ret;
}

// recode an upper case DNA sequence to CTR
function recode_ctr(dna)
{
	let ret = new Buffer(dna);

	for (let i=0; i<ret.length; ++i)
	{
		switch (ret[i]) {
			case cA: ret[i] = cR; break;
			case cG: ret[i] = cR; break;
		}
	}
	return ret;
}

// recode an upper case DNA sequence to RY
function recode_ry(dna)
{
	let ret = new Buffer(dna);

	for (let i=0; i<ret.length; ++i)
	{
		switch (ret[i]) {
			case cA: ret[i] = cR; break;
			case cT: ret[i] = cY; break;
			case cC: ret[i] = cY; break;
			case cG: ret[i] = cR; break;
		}
	}
	return ret;
}

// extract sequences from a FastA file
function load_fasta(ifile)
{
	let reader = new LineReader(ifile);
	let line = null;
	let meta = null;
	let data = [];
	let records = [];

	// add records
	while (line = reader.next())
	{
		line = line.toString().trim();

		if (line[0] == '>') {
			if (meta) records.push({ meta, data : data.join('') });
			meta = line;
			data = [];
		}
		else {
			data.push(line.toUpperCase());
		}
	}

	// write the residual
	if (meta) records.push({ meta, data : data.join('') });

	return records;
}

// force a DNA sequence to only contain ATCG or N
function force4(seq)
{
	let a = 'a'.charCodeAt(0);
	let t = 't'.charCodeAt(0);
	let c = 'c'.charCodeAt(0);
	let g = 'g'.charCodeAt(0);

	let A = 'A'.charCodeAt(0);
	let T = 'T'.charCodeAt(0);
	let C = 'C'.charCodeAt(0);
	let G = 'G'.charCodeAt(0);
	let N = 'N'.charCodeAt(0);

	let buf = new Buffer(seq);

	for (let i=0; i<buf.length; ++i) {
		switch (buf[i]) {
			case A: break;
			case T: break;
			case C: break;
			case G: break;
			case N: break;
			case a: buf[i] = A; break;
			case t: buf[i] = T; break;
			case c: buf[i] = C; break;
			case g: buf[i] = G; break;
			default: buf[i] = N;
		}
	}
	return buf;
}

// count CGs in a sequence
function count_cgs(seq)
{
	let buf = new Buffer(seq);
	let C = 'C'.charCodeAt(0);
	let G = 'G'.charCodeAt(0);
	let c = 'c'.charCodeAt(0);
	let g = 'g'.charCodeAt(0);
	let count = 0;

	for (let b of buf) {
		if (b == C || b == G || b == c || b == g) count++;
	}
	return count;
}

module.exports = { force4, clean, revcomp, recode_agy, recode_ctr, recode_ry, load_fasta, count_cgs };
