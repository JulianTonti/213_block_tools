/*

# Day 324: 2017-Nov-21 (Tue):

Index that allows forward and backwards search at the expense of extra memory.

Forward search requires the SA & sequence
Reverse search requires the A->B rank mapping

*/

// wrap a value to keep it within a range
function wrap(min,maxex,val) {
	if (val >= min && val < maxex) return val;
	let mod = (val-min) % (maxex-min);
	return mod >= 0 ? min+mod : maxex+mod;
}

// find any matching element
function binary_search(min,max,test) {
	while (min <= max) {
		let mid = min + (max - min >>> 1);
		let res = test(mid);
		if (res == 0) return mid;
		res > 0 ? min = mid + 1 : max = mid - 1;
	}
	return -1;
}

// find the first matching element
function binary_search_head(min,max,test) {
	while (min <= max) {
		let mid = min + (max - min >>> 1);
		test(mid) > 0 ? min = mid + 1 : max = mid - 1;
	}
	return test(++max) == 0 ? max : -1;
}

// find the last matching element
function binary_search_tail(min,max,test) {
	while (min <= max) {
		let mid = min + (max - min >>> 1);
		test(mid) < 0 ? max = mid - 1 : min = mid + 1;
	}
	return test(--min) == 0 ? min : -1;
}

// find the entire range of matching elements
function binary_search_range(min,max,test) {
	while (min <= max) {
		let mid = min + (max - min >>> 1);
		let res = test(mid);
		if (res == 0) return [binary_search_head(min,mid,test), binary_search_tail(mid,max,test)];
		res > 0 ? min = mid + 1 : max = mid - 1;
	}
	return [-1,-1];
}

// generate a suffix array for a buffer
function blocksort(buffer)
{
	let size = buffer.length;

	// initialize memory. By default these are all 0-filled
	let posn_to_rank = new Uint32Array(size).fill(0); // position to rank. Evolves into the Inverse Suffix Array when all ranks are unique (one member per ranking)
	let rank_to_oset = new Uint32Array(size).fill(0); // rank to offset. Evolves into 1:1 when all ranks are unique
	let oset_to_posn = new Uint32Array(size).fill(0); // offset to position. Evolves into the Suffix Array when all ranks are unique
	let swap_space   = new Uint32Array(size).fill(0); // swap space

	// count the numbers of each octet
	let counts = new Uint32Array(256).fill(0);

	for (let b of buffer) {
		counts[b]++;
	}

	// map octets to ranks and count the total number of ranks
	let octet_to_rank = new Uint32Array(256).fill(0);
	let numranks = 0;

	for (let octet=0; octet<256; ++octet) {
		if (counts[octet] == 0) continue;
		octet_to_rank[octet] = numranks++;
	}

	// convert from counts by octet to counts by rank
	for (let octet=0; octet<256; ++octet) {
		if (counts[octet] == 0) continue;
		counts[octet_to_rank[octet]] = counts[octet];
	}

	// populate posn_to_rank
	for (let posn=0; posn<size; ++posn) {
		posn_to_rank[posn] = octet_to_rank[buffer[posn]];
	}

	// populate rank_to_oset
	for (let rank=0,oset=0; rank<numranks; ++rank) {
		rank_to_oset[rank] = oset;
		oset += counts[rank];
	}

	// populate oset_to_posn (first radix sort)
	for (let posn=0; posn<size; ++posn) {
		let rank = posn_to_rank[posn];
		let oset = rank_to_oset[rank]++; // increment the offset pointer after using it
		oset_to_posn[oset] = posn;
	}

	// revert rank_to_oset
	for (let rank=0; rank<numranks; ++rank) {
		rank_to_oset[rank] -= counts[rank];
	}

	// the outer loop is the log(n) component, the 3 inner loops are the (n) components
	for (let iteration=1; numranks<size && iteration<size; iteration *= 2)
	{
		// LEFT SHIFT each position in the ordered array by 'iteration' places, writing into swap_space
		for (let oset=0; oset<size; ++oset) {
			let posn = oset_to_posn[oset];
			swap_space[oset] = (posn >= iteration) ? posn - iteration : size - (iteration - posn); // wraps around if needed
		}

		// RADIX SORT BY GROUP from swap_space back into oset_to_posn (which is then complete for this round)
		for (let i=0; i<size; ++i) {
			let posn = swap_space[i];
			let rank = posn_to_rank[posn];
			let oset = rank_to_oset[rank]++; // increment the offset after reading it
			oset_to_posn[oset] = posn;
		}

		// REASSIGN GROUPS identify unique pairs of sorted rank-groups (separated by 'iteration' distance) and assign new ranks
		let prev_rankA = 0;
		let prev_rankB = 0;

		numranks = 0; // reset the rank counter

		for (let oset=0; oset<size; ++oset)
		{
			let posnA = oset_to_posn[oset];
			let posnB = (size - posnA > iteration) ? posnA + iteration : iteration - (size - posnA); // wrap around if needed

			let rankA = posn_to_rank[posnA];
			let rankB = posn_to_rank[posnB];

			// if the two ranks differ from the previous ranks then assign a new rank
			if (rankA != prev_rankA || rankB != prev_rankB || numranks == 0)
			{
				prev_rankA = rankA;
				prev_rankB = rankB;
				rank_to_oset[numranks] = oset; // assign the new group and set its offset (start)
				++numranks;
			}

			// write into swap_space as a proxy for posn_to_rank
			swap_space[posnA] = numranks - 1;
		}

		// update posn_to_rank by swapping it with swap_space
		let temp = posn_to_rank;
		posn_to_rank = swap_space;
		swap_space = temp;
		//console.log(iteration);
	}

	// oset_to_posn has now evolved into rank_to_posn, ie, the suffix array
	return oset_to_posn;
}

// affix index (allows F and R search)
function Index(str)
{
	if (this.constructor != Index) return new Index(str);

	let buf = null; // sequence buffer
	let len = 0; // length of the buffer
	let counts = new Uint32Array(256).fill(0); // counts for each octet
	let starts = new Uint32Array(256).fill(0); // start of each octet range in column A
	let sa = null; // suffix array
	let ab = null; // A->B mapping (rank -> rank of next character)

	// build the index
	this.build = function build(str)
	{
		buf = new Buffer(str);
		len = buf.length;

		// build counts for each octet
		counts.fill(0);
		for (let i=0; i<len; ++i) {
			counts[buf[i]] += 1;
		}

		// set starts for each octet range
		starts.fill(0);
		for (let start=0, i=0; i<256; ++i) {
			starts[i] = start;
			start += counts[i];
		}

		// build the suffix array (rank -> posn)
		sa = blocksort(buf);

		// build the inverse suffix array (posn -> rank)
		let isa = new Uint32Array(len);
		for (let rank=0; rank<len; ++rank) {
			let posn = sa[rank];
			isa[posn] = rank;
		}

		// build the AB mapping (rank -> rank of next character)
		ab = new Uint32Array(len);
		for (let rank=0; rank<len; ++rank) {
			let posn = sa[rank];
			let next_posn = ++posn == len ? 0 : posn;
			ab[rank] = isa[next_posn];
		}
	}

	// given an existing blockrange, refine downstream for a particular octet (SEQ & SA)
	this.search_next = function search_next(octet, minrow, maxrow, col=0)
	{
		if (counts[octet] == 0) return [-1,-1];

		function test(row) {
			return octet - buf[wrap(0, len, sa[row] + col)];
		}
		return binary_search_range(minrow, maxrow, test);
	}

	// given an existing blockrange, refine upstream for a particular octet (A & AB)
	this.search_prev = function search_prev(octet, minrow, maxrow)
	{
		if (counts[octet] == 0) return [-1,-1];

		let test = function(row) {
			if (ab[row] < minrow) return 1;
			if (ab[row] > maxrow) return -1;
			return 0;
		}
		return binary_search_range(starts[octet], starts[octet] + counts[octet] - 1, test);
	}

	// given an existing alignment, extend it as far as possible downstream (requires SA & seq)
	this.extend_dn = function extend_dn(buffer,best) //buffer, {pos,len,min,max}
	{
		let pos = best.pos;

		while (best.len < buffer.length)
		{
			let [min,max] = this.search_next(buffer[pos], best.min, best.max, best.len);
			if (min == -1) break;
			best.min = min;
			best.max = max;
			best.len++;
			pos++;
			if (pos == buffer.length) pos = 0;
		}
	}

	// given an existing alignment, extend it as far as possible upstream (requires A & AB)
	this.extend_up = function extend_up(buffer,best)
	{
		let pos = best.pos;

		while (best.len < buffer.length)
		{
			pos = (pos == 0) ? buffer.length - 1 : pos - 1;
			let [min,max] = this.search_prev(buffer[pos], best.min, best.max);
			if (min == -1) break;
			best.min = min;
			best.max = max;
			best.pos = pos;
			best.len++;
		}
	}

	// given a basis buffer and offset position, find an alignment down then up
	this.search = function search(buffer,offset)
	{
		let alignment = {
			pos : offset,
			len : 0,
			min : 0,
			max : len - 1
		};
		this.extend_dn(buffer,alignment);
		this.extend_up(buffer,alignment);
		return alignment;
	}

	// given a rank, return the genomic position for that rank
	this.get_pos = function get_pos(rank)
	{
		return sa[rank];
	}

	// construct
	if (str) this.build(str);
}
module.exports = Index;
