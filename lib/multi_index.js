/*

# Day 327: 2017-Nov-24 (Fri):

An index that wraps a collection of affix arrays to provide a single interface

*/
const AffixArray = require('./affix_array');

function MultiIndex()
{
	if (this.constructor != MultiIndex) return new MultiIndex();

	let indexes = {};
	let ids = [];

	// add a sequence to the index
	function add_sequence(id,buffer)
	{
		if (indexes[id]) {
			return console.error('index ID already used:',id);
		}
		ids.push(id);
		indexes[id] = new AffixArray(buffer);
	}

	// search for the best alignment (by longest length)
	function search(buffer, offset)
	{
		let best = 0;
		let alignments = [];

		for (let id of ids)
		{
			let alignment = indexes[id].search(buffer,offset);

			if (alignment.len < best) {
				continue;
			}
			if (alignment.len > best) {
				alignments = [];
				best = alignment.len;
			}
			alignment.id = id;
			alignments.push(alignment);
		}
		console.assert(alignments.length > 0);

		return alignments;
	}

	// given an alignment, expand it into positions
	function expand(alignments)
	{
		let positions = [];

		for (let alignment of alignments)
		{
			for (let rank=alignment.min; rank<=alignment.max; ++rank)
			{
				positions.push([
					alignment.pos,
					alignment.len,
					alignment.id,
					rank_to_posn(alignment.id,rank)
				]);
			}
		}
		return positions;
	}

	// convert a rank to a position (requires the ID of the sequence)
	function rank_to_posn(id,rank)
	{
		return indexes[id].get_pos(rank);
	}

	// get a base at a specific position
	function posn_to_base(id,posn)
	{
		return indexes[id].get_pos(posn);
	}

	return { add_sequence, search, expand, rank_to_posn, posn_to_base };
}
module.exports = MultiIndex;

// tests
if (require.main === module)
{
	let fwd = new Buffer('ATCG');
	let rev = new Buffer('CGAT');

	let index = new MultiIndex();
	index.add_sequence('+',fwd);
	index.add_sequence('-',fwd);

	// [ 0, 0 ]
	console.log(index.search_all(new Buffer('A'),0).map(h => index.get_pos(h.id,h.min)));

	// [ [ 0, '+', 4 ], [ 0, '-', 4 ] ]
	console.log(index.search_all(new Buffer('ATCG'),0).map(h => [index.get_pos(h.id,h.min),h.id,h.len]));
}
