/*

# Day 24: 2017-Jan-25 (Wed):

Wrap a number (min is inclusive, max is exclusive)

Two additional helper methods are provided

wrap.get
wrap.set

*/
function wrap(min,maxex,val) {
	if (val >= min && val < maxex) return val;
	let mod = (val-min) % (maxex-min);
	return mod >= 0 ? min+mod : maxex+mod;
}
wrap.get = function(data,pos) {
	return data[wrap(0,data.length,pos)];
}
wrap.set = function(data,pos,val) {
	return data[wrap(0,data.length,pos)] = val;
}
module.exports = wrap;
