/*

# Day 184: 2017-Jul-04 (Tue): created
# Day 259: 2017-Sep-17 (Sun): refactored and simplified

Read lines from a file. Works by loading file in chunks

*/
"use strict";

const fs = require('fs');
const BUFSIZE = Math.pow(2,16);

function LineReader(ifile)
{
	let fsize, fd, buffer, lines, residual, counter = null;
	if (ifile) open(ifile);

	function open(ifile)
	{
		close();
		fsize  = fs.statSync(ifile).size;
		fd     = fs.openSync(ifile,'r');
		buffer = new Buffer(BUFSIZE);
	}

	function close()
	{
		if (fd) fs.closeSync(fd);
		fsize    = 0;
		fd       = null;
		buffer   = null;
		lines    = [];
		residual = '';
		counter  = 0;
	}

	function next()
	{
		while (lines.length == 0 && fd)
		{
			let bytes_read = fs.readSync(fd, buffer, 0, BUFSIZE);

			// if finished reading, close the file and adjust the final buffer
			if ((counter += bytes_read) >= fsize)
			{
				fs.closeSync(fd);
				fd = null;
				buffer = buffer.slice(0,bytes_read);
			}
			lines = buffer.toString().split(/[\r\n]+/);
			lines[0] = residual + lines[0];
			residual = fd ? lines.pop() : '';
		}
		return lines.length == 0 ? null : lines.shift();
	}
	return { open, close, next };
}
module.exports = LineReader;

if (require.main === module)
{
	function test()
	{
		console.log(Date(),'started');
		const fs = require('fs');
		const sh = require('shelljs');

		let ifile = "/Volumes/owc-data/dl/web.corral.tacc.utexas.edu/OneKP/AALA-Meliosma_cuneifolia/solexa-reads/AALA-read_1.fq";
		let reader = new LineReader(ifile);
		let writer = new require('./line_writer')('./out');
		let counter = 0;

		for (let line=reader.next(); line !== null; line=reader.next())
		{
			//writer.next(line);
			//console.log(line);
			if (++counter > 23772850) console.log(counter, line);

		}
		writer.close();
		console.log(Date(),'finished',counter);
	}
	test();
}
