/*

# Day 184: 2017-Jul-04 (Tue):

Synchronous line writer with some internal buffering.

Send null to flush the buffer and close the file

# Day 199: 2017-Jul-19 (Wed):
Fixed a bug that was preventing buffering after the first flush

*/
const fs = require('fs');
const MAXSIZE = Math.pow(2,20);

function LineWriter(ofile)
{
	let stack=[];
	let size = 0;
	let fd = null;

	if (ofile) open(ofile);

	// open a new writer
	function open(ofile)
	{
		close();
		fd = fs.openSync(ofile,'w');
	}

	// flush the stack and terminate the writer
	function close()
	{
		if (stack.length > 0 && fd) {
			fs.writeSync(fd, Buffer.concat(stack));
			stack = [];
		}
		if (fd) fs.closeSync(fd);
		fd = null;
		stack = [];
		size = 0;
	}

	// write the next line
	function next(buf)
	{
		if (fd === null) throw "cannot write to finished LineWriter";

		// sending null is an instruction to flush and finish
		if (buf === null) {
			return close();
		}

		// add the line to the internal buffer
		stack.push(new Buffer(buf));
		stack.push(new Buffer("\n"));

		size += buf.length + 1;

		// flush the buffer if it has hit the size limit
		if (size >= MAXSIZE) {
			fs.writeSync(fd, Buffer.concat(stack));
			stack = [];
			size = 0;
		}
	}
	return { open, close, next };
}
module.exports = LineWriter;


//TODO: this test case adds an extra \n at the end. Figure out why
function test()
{
	let writer = new LineWriter("./test");

	let lines = fs.readFileSync(__filename).toString().split("\n");
	for (let line of lines) writer.next(line);
	writer.next(null);
	//writer.next(); //should throw
}
if (require.main === module) test();
